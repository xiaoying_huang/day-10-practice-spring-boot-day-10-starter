## Objective 事实
- Learned to use Spring Boot DTOs to map between DATA and Object.
- Learned to use Spring Boot Mapper to map between Entities and DTOs.
- Learned how to use Flyway to manage DB scripts instead of hosting with JPA.
- Learned what is Retrospective Meeting and how to attend it.
- Learned about Microservice, Container, CI/CD by Teamwork Showcase.

## Reflective 反应
- I can quick to get started.
- It's a good way for Retro Meeting to identify the strengths and weaknesses of the team and make us trust each other more.

## Interpretive 解释/分析
- It's easy understand the logic that why we should use DTOs and Mapper so that I can quick to get started.

## Decision 决定/结果
- It is a very useful technique to practice after understanding the logic.


