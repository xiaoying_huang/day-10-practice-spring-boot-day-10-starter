package com.afs.restapi.dto;

import com.afs.restapi.entity.Employee;

import java.util.List;

public class CompanyCreateRequest {
    private String name;
    private List<Employee> employees;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public CompanyCreateRequest() {

    }
}
