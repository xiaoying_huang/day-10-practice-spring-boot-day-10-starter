package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.entity.Company;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {
    public static CompanyResponse toResponse(Company company) {
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company, companyResponse);
        companyResponse.setEmployeeNumbers(company.getEmployees().size());
        return companyResponse;
    }

    public static Company toEntity(CompanyCreateRequest CompanyCreateRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(CompanyCreateRequest,company);
        return company;
    }
}