package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return employeeRepository.findAllByStatusTrue()
                .stream()
                .map(EmployeeMapper::toResponse)
                .collect(toList());
    }

    public EmployeeResponse update(int id, EmployeeUpdateRequest toUpdateEmployee) {
        Employee employee = findById(id);
        if (toUpdateEmployee.getAge() != null) {
            employee.setAge(toUpdateEmployee.getAge());
        }
        if (toUpdateEmployee.getSalary() != null) {
            employee.setSalary(toUpdateEmployee.getSalary());
        }
        employeeRepository.save(employee);
        return EmployeeMapper.toResponse(employee);
    }

    public Employee findById(int id) {
        return employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGenderAndStatusTrue(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return employeeRepository.findAllByStatusTrue(pageRequest).toList();
    }

    public EmployeeResponse insert(EmployeeCreateRequest employeeCreateRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeCreateRequest);
        employeeRepository.save(employee);
        return EmployeeMapper.toResponse(employee);
    }

    public void delete(int id) {
        Employee employee = findById(id);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
