package com.afs.restapi.controller;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.DisplayName.class)
class CompanyControllerTest {

    @Autowired
    MockMvc client;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    CompanyRepository companyRepository;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void prepareData() {
        companyRepository.deleteAll();
        employeeRepository.deleteAll();
    }

    @Test
    void should_list_all_company_with_employee_numbers_when_preform_get_given_company_and_employee_in_db() throws Exception {
        //given
        Company aGreatCompany = buildGreatCompany();
        Employee employeeSusan = buildEmployeeSusan(aGreatCompany);
        Employee employeeLisi = buildEmployeeLisi(aGreatCompany);
        aGreatCompany.setEmployees(List.of(employeeSusan, employeeLisi));
        aGreatCompany = companyRepository.save(aGreatCompany);

        Company anotherGreatCompany = buildAnotherGreatCompany();
        Employee employeeOfAnother = buildEmployeeAnotherCompany(anotherGreatCompany);
        anotherGreatCompany.setEmployees(List.of(employeeOfAnother));
        anotherGreatCompany = companyRepository.save(anotherGreatCompany);
        //when
        client.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(aGreatCompany.getId()))
                .andExpect(jsonPath("$[0].name").value(aGreatCompany.getName()))
                .andExpect(jsonPath("$[0].employeeNumbers").value(2))
                .andExpect(jsonPath("$[1].id").value(anotherGreatCompany.getId()))
                .andExpect(jsonPath("$[1].name").value(anotherGreatCompany.getName()))
                .andExpect(jsonPath("$[1].employeeNumbers").value(1));
    }

    @Test
    void should_return_company_when_perform_given_company_in_db() throws Exception {
        //given
        Company aGreatCompany = buildGreatCompany();
        Company anotherGreatCompany = buildAnotherGreatCompany();
        Employee employeeSusan = buildEmployeeSusan(aGreatCompany);
        Employee employeeLisi = buildEmployeeLisi(aGreatCompany);
        aGreatCompany.setEmployees(List.of(employeeSusan, employeeLisi));
        aGreatCompany = companyRepository.save(aGreatCompany);
        companyRepository.save(anotherGreatCompany);


        //when
        client.perform(MockMvcRequestBuilders.get("/companies/{id}", aGreatCompany.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(aGreatCompany.getId()))
                .andExpect(jsonPath("$.name").value(aGreatCompany.getName()))
                .andExpect(jsonPath("$.employees", hasSize(2)))
                .andExpect(jsonPath("$.employees[0].name").value(employeeSusan.getName()))
                .andExpect(jsonPath("$.employees[0].age").value(employeeSusan.getAge()))
                .andExpect(jsonPath("$.employees[0].gender").value(employeeSusan.getGender()))
                .andExpect(jsonPath("$.employees[0].salary").value(employeeSusan.getSalary()))
                .andExpect(jsonPath("$.employees[1].name").value(employeeLisi.getName()))
                .andExpect(jsonPath("$.employees[1].age").value(employeeLisi.getAge()))
                .andExpect(jsonPath("$.employees[1].gender").value(employeeLisi.getGender()))
                .andExpect(jsonPath("$.employees[1].salary").value(employeeLisi.getSalary()));

        //then
    }

    @Test
    void should_return_employees_when_perform_get_by_page_given_employees() throws Exception {
        // given
        Company aGreatCompany = buildGreatCompany();
        Company anotherGreatCompany = buildAnotherGreatCompany();
        aGreatCompany = companyRepository.save(aGreatCompany);
        anotherGreatCompany = companyRepository.save(anotherGreatCompany);
        Employee employeeSusan = buildEmployeeSusan(aGreatCompany);
        Employee employeeLisi = buildEmployeeLisi(aGreatCompany);
        Employee employeeOfAnother = buildEmployeeAnotherCompany(anotherGreatCompany);
        employeeRepository.saveAll(List.of(employeeSusan, employeeLisi, employeeOfAnother));

        // when
        client.perform(MockMvcRequestBuilders.get("/companies")
                        .param("page", "1")
                        .param("size", "1")
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name").value(anotherGreatCompany.getName()))
                .andExpect(jsonPath("$[0].employees", hasSize(1)))
                .andExpect(jsonPath("$[0].employees[0].name").value(employeeOfAnother.getName()))
                .andExpect(jsonPath("$[0].employees[0].age").value(employeeOfAnother.getAge()))
                .andExpect(jsonPath("$[0].employees[0].gender").value(employeeOfAnother.getGender()))
                .andExpect(jsonPath("$[0].employees[0].salary").value(employeeOfAnother.getSalary()));
        // should
    }

    @Test
    void should_return_employees_when_perform_get_employees_by_company_given_company_in_db_and_company_id() throws Exception {

        // given
        Company aGreatCompany = buildGreatCompany();
        Company anotherGreatCompany = buildAnotherGreatCompany();
        Employee employeeSusan = buildEmployeeSusan(aGreatCompany);
        Employee employeeLisi = buildEmployeeLisi(aGreatCompany);
        Employee employeeOfAnother = buildEmployeeAnotherCompany(anotherGreatCompany);
        aGreatCompany.setEmployees(List.of(employeeSusan, employeeLisi));
        aGreatCompany = companyRepository.save(aGreatCompany);
        anotherGreatCompany.setEmployees(List.of(employeeOfAnother));
        companyRepository.save(anotherGreatCompany);

        //when
        client.perform(MockMvcRequestBuilders.get("/companies/{id}/employees", aGreatCompany.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name").value(employeeSusan.getName()))
                .andExpect(jsonPath("$[0].age").value(employeeSusan.getAge()))
                .andExpect(jsonPath("$[0].gender").value(employeeSusan.getGender()))
                .andExpect(jsonPath("$[0].salary").value(employeeSusan.getSalary()))
                .andExpect(jsonPath("$[1].name").value(employeeLisi.getName()))
                .andExpect(jsonPath("$[1].age").value(employeeLisi.getAge()))
                .andExpect(jsonPath("$[1].gender").value(employeeLisi.getGender()))
                .andExpect(jsonPath("$[1].salary").value(employeeLisi.getSalary()));
    }

    @Test
    void should_return_company_and_save_to_db_when_perform_post_given_company() throws Exception {
        //given
        Company aGreatCompany = buildGreatCompany();
        Employee employeeSusan = buildEmployeeSusan(aGreatCompany);
        Employee employeeLisi = buildEmployeeLisi(aGreatCompany);
        aGreatCompany.setEmployees(List.of(employeeSusan, employeeLisi));

        //when
        client.perform(post("/companies").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(aGreatCompany)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(aGreatCompany.getName()))
                .andExpect(jsonPath("$.employeeNumbers").value(2));

        List<Company> companies = companyRepository.findAll();
        assertEquals(aGreatCompany.getName(), companies.get(0).getName());
    }

    @Test
    void should_edit_company_when_perform_put_given_company() throws Exception {
        //given
        Company aGreatCompany = buildGreatCompany();

        Employee employeeSusan = buildEmployeeSusan(aGreatCompany);
        Employee employeeLisi = buildEmployeeLisi(aGreatCompany);
        aGreatCompany.setEmployees(List.of(employeeSusan, employeeLisi));
        aGreatCompany = companyRepository.save(aGreatCompany);
        Company putInfo = new Company();
        BeanUtils.copyProperties(aGreatCompany, putInfo);
        putInfo.setName("New Name");
        //when
        client.perform(put("/companies/{id}", aGreatCompany.getId()).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(putInfo)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(putInfo.getName()))
                .andExpect(jsonPath("$.employeeNumbers").value(2));

        List<Company> companies = companyRepository.findAll();
        assertEquals("New Name", companies.get(0).getName());
    }

    @Test
    void should_del_company_when_perform_del_given_id() throws Exception {
        //given
        Company aGreatCompany = buildGreatCompany();
        Employee employeeSusan = buildEmployeeSusan(aGreatCompany);
        Employee employeeLisi = buildEmployeeLisi(aGreatCompany);
        aGreatCompany.setEmployees(List.of(employeeSusan, employeeLisi));
        aGreatCompany = companyRepository.save(aGreatCompany);

        //when
        client.perform(delete("/companies/{id}",aGreatCompany.getId()))
                .andExpect(status().isNoContent());
        //then
        assertNull(companyRepository.findById(aGreatCompany.getId()).orElse(null));
    }

    private static Employee buildEmployeeAnotherCompany(Company anotherGreatCompany) {
        return new Employee(null, "Wangwu", 25, "Man", 6000, anotherGreatCompany.getId(), true);
    }

    private static Employee buildEmployeeLisi(Company aGreatCompany) {
        return new Employee(null, "Lisi", 25, "Man", 3000, aGreatCompany.getId(), true);
    }

    private static Employee buildEmployeeSusan(Company aGreatCompany) {
        return new Employee(null, "Susan", 33, "Female", 1000000, aGreatCompany.getId(), true);
    }

    private static Company buildAnotherGreatCompany() {
        return new Company(null, "Another Great Company");
    }

    private static Company buildGreatCompany() {
        return new Company(null, "A Great Company");
    }
}